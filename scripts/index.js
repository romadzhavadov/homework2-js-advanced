const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

  class Book {
    constructor(obj){
        this.obj = obj;
    };

    isValidBook() {
        if (!Object.keys(this.obj).includes("author")) {
            throw new InvalidProperty('author');
          }
          if (!Object.keys(this.obj).includes("name")) {
            throw new InvalidProperty('name');
          }
          if (!Object.keys(this.obj).includes("price")) {
            throw new InvalidProperty('price');
          }
    }

    createElement() {
        const root = document.querySelector('#root');
        const rootItem = document.createElement('ul');
        const rootLi = document.createElement('li');
        root.append(rootItem);
        rootItem.append(rootLi)
        rootLi.innerHTML = `${this.obj.author}: ${this.obj.name}`
    }
    render() {
        this.createElement()
        this.isValidBook()
    }
  }


  class InvalidProperty extends Error {
    constructor(book) {
        super();
        this.name = 'Missing';
        this.message = `Invalid property: ${book}`
    }
}

  books.forEach((e) => {
    try {
        new Book(e).render();
    } catch (err) {
        if (err.name === 'Missing') {
            console.log(err)
        } 
        else {
            throw err;
        }
    }
  })


